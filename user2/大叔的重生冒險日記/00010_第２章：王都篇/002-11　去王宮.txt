在異世界的第23日。昨晚在迷宮都市的街上訂了旅館。雖然沒有適合王族住宿的旅館，但是他們好像安排了住宿過的地方。

王宮已經快馬先行通知了，並且派遣了騎士團。愛德白白的燃燒殆盡。太好了。沒有白費3天都背著王子走路的經驗了。

第二天，王子一行人乘坐豪華的馬車去了王都。我們也被邀請去王宮。

在王宮的一個房間。在豪華的房間裡等著，大家好像都靜不下心來。我呢，心情很好，尾巴啪嗒啪嗒的狀態。

被帶到這裡，是不想單方面地違反約定。爭取參觀最差的寶物庫。也許可以複製什麼好東西。興奮不已。

「你可真那麼隨心所欲啊。是王宮嗎？」
「所以啊？」
愛德被我那高興的樣子傷腦筋。

「這裡本來不是我們來這裡啊？」
「是啊～。」
還有更讓人頭疼的傢伙。

「什麼？不會被殺的。無論背後如何表面上都是王子的救命恩人吧？殺過來的話，只有打開血路。什麼，這個王國也不小氣。」

「這不是小氣之類的問題……」
愛德說放棄了。

康康。哎呀？好像有人來了。如果這是網路笑話，應該就是【Fin】，敲門後門開了，白銀鎧甲的騎士進來說話。（譯註：Fin，結局。）

「國王陛下要接見你們。」

「！」
突然朝見國王啊！這個出乎意料啊。

大家都變得緊張得厲害了。我不是這個世界的人，所以完全沒有真實感。左顧右盼。

（做得真好。想複製。）
到底，沒有那樣的餘裕。

「我帶了5名冒險者過來！」

門開了，被通往裡面。啊，氣氛很糟糕。不是危險什麼的。總覺得那裡的人的氣氛。我深深地感受到，自己不能在這裡，這種不合時宜的感覺。

嘛，事到如今才為時已晚。我歎息著，要是換上稍微好點的衣服就好了。

典型的冒險者風格的我們，用不習慣的腳步穿過華麗的各位的御前。全體人員即使指示了跪下低著頭。

「不要覺得不舒服。抬起頭來。」
哦～！竟然說出那種話來。有點感動了。

「這次，很好地救了艾米利奧。非常感謝。」
「哈哈。感謝的話。非常感激。」
鄭重地，禮貌地。

「有時候為了獎勵，想要寶物庫的東西吧？那是全體人員嗎？」
看愛德他們。全體人員紛紛搖頭。

「他們說不需要。本來是我提出這個要求的，殿下的護衛就拜託給他們作為C級隊伍適當的報酬。」

「這樣啊。但是寶物庫的東西，也不能那麼簡單地交給你。」

「這樣啊。那麼能讓我參觀寶物庫嗎？作為一名商人，希望一生中能有一次這樣的眼福。」

「哦。那是沒有慾望的事情。」
國王陛下，像稍微考慮一樣地之後說。
「知道了。就那麼安排吧。」

「真的嗎？難得的幸福！」
「那就退了吧。」

「哈哈。」
太好了，獲得權利！我高興得想當場跳起舞來。雖然不怎麼會跳舞。

和愛德他們在這裡分手了。
「再見～。」

我滿面笑容，豎起大拇指目送著。
「謝謝。希望下次能再一起哦。」

「蛋糕。奶油泡芙、布丁。」
有誰在說什麼啊。真的哭了。

那裡，真是壯觀的景色。王宮寶物庫。通過嚮導和監視的騎士兩名嚮導可以進去。

寶物庫，被那個聲音陶醉了。看過3個人的地位，但是沒有特別的技能。但是，如果做出奇怪的模仿的話，騎士們是不會放手的吧。

呵呵。雖然很遺憾，但是可以通過目視收納瞬間複製，不用擔心～。

如果能找到任何的武器、防具或魔道具，如果是自己的能力說不定能複製和再現。因為有這樣的想法，參觀時非常認真。

發生了很多事情。確實成為了參考。這種形式是追求這種功能的嗎？有這樣的功能的魔道具嗎？以那樣的目的大概就是這樣的構造吧！

在鎧甲的裝飾上也記錄下在磁碟。能複製的東西就要複製。

最近太大膽了。如果有察覺到魔力並告知的警報的話，到底打算怎麼辦呢？但是，我的搭檔第七感沒有發出警告，不過。

最裡面是初代建國王的遺物。不過，在那裡「轉移的手鐲」被裝飾著。我好像被吞噬了一樣。不湊巧的是無法複製的東西。豈止如此，連瞬間收納都做不到。遺憾萬分。

試著解析。過去魔力的流動，為了看到魔法的發動而集中精力……然後不知為什麼看到了。

或許是因為手鐲中注入的稀人魔力吧。感覺那一瞬間就像是永遠。然後，手鐲的複製品竟然用【形象創造】做成了。

也許，第一代國王會為我們考慮，如果稀人來的話就能看到。

一邊看實物一邊進行各種檢查。重玩一次試試比較，直到心滿意足為止。畢竟是個引人注目的地方，即使花時間也沒有人發牢騷。

同時以第七感領悟到絕望。清楚地感到用這個手鐲回地球，是不可能的。

仔細一想。明明擁有這樣的東西，那個國王卻沒能回到地球。

大概是本人製作的魔法道具無法返回，這個王國現在才存在。沒注意到那個的自己太興奮了。

嘛，因為得到了好的裝備才好吧。這樣的機會不會有第二次了吧。轉換心情，此後也專心地複製。已經複製8成了。是個壞蛋。

雖然無法複製附帶的物品和擁有麻煩魔力的魔道具等，但能夠模仿後製作出一些類似的東西。

那一帶在當地也占了三成。用答錄機好好地記錄著解析專心致志的身姿。因為在實物面前做著，這個那個地成為了參考。

作為報酬足夠有餘。已經久留了6個小時給添麻煩了。但是，大家還是對我說謝謝幫助了殿下。胸口有點疼啊。向嚮導道謝，壞蛋退了出去。

我非常高興地離開城堡考慮之後，決定去北方的迷宮。在那之前我決定了去公會進行提升等級的談判。

和公會長商量了一下，突然被他抱住了頭。

「單獨討伐A級的魔物，就算是B級的冒險者也不可能。是A級推薦案件。但是，你是E級。本來是不可能的。」

暫時盯著天花板後，公會長好像下定了決心。

「喂！我會把你升到C級的，你就先忍耐一下吧。」
把手貼在額頭上，公會長粗暴地吐出話來。

「哦！真的可以嗎？我還以為是D級。」
這是令人高興的誤算。有跳級制度嗎？

「可以嗎？如果只是實力的話，你就是A級。但是。A級和B級不通過試驗是不能提高的。用公會長的許可權，無條件能提高到C級為止。因此C級。作為代替，給B級試驗的公會長推薦。所以忍耐吧。有這個的話，無條件進入B級試驗的優先權。你方便的話，明天有升級的試驗，你去參加吧。」

突然明天嗎？公會長繼續向我解釋。

「B級試驗在各國舉行。一次大約五百人。一個月左右的頻率。在預選賽中，將20名前B級教官作為對手，然後進行淘汰賽。但是，不獲勝是不行的，而且是審查如果不能合格就不行的1次合格者能不能出1人的狹窄的門。

但是，合格後就變成【相當於男爵的待遇】。也可以申請到王國侍奉國家。只要侍奉國家，就會被授予名譽男爵位。如果提高功績正式的爵位也被給予。據說有領地的人也不是夢。

如果殺了對方就失去資格。因此很少丟掉生命。不過，不是絕對的。有的人因為這個而引退，有的人因為敗犬的毅力而一直處於C級。

A級的話在大陸各國輪流舉辦。現役B級在全世界有200多人。已經到了B級的年齡，引退的傢伙也很多。所以A級試驗每次都用同樣的方式進行。在那裡優勝了的傢伙無條件升為A級。這是子爵待遇啊。只是，還是也有年齡的關係。A級現在世界上不是12～13人嗎？至於S級，現役只有3人左右。

如果你明天能獲勝，我強烈地推薦你升到B級。也能推薦參加A級試驗。怎麼樣？」

「嗯～。」
「你啊。」
公會長發出了驚訝的聲音。

哎呀，確實是那樣的東西吧。在這個世界上，貴族站在平民之上是基本。但是，在日本出生的我，沒有理會到這一點也是理所當然的。

「啊，先接受吧。」
「是嗎？別遲到了。從早上九點開始。」
「是～。」
「哎呀哎呀。」

真是麻煩的事啊。應該達到了目標的C級。貴族啊。怎麼說呢。但是貴族待遇也不壞啊。在旅館的待遇說不定好。還說了要出去呢。

試著做了試用的武器。不能殺啊。與其說是麻痺不如說是給予了使之昏倒的魔法【昏厥】的武器系列之類。手槍之類也試著做了。

我有航空電子的連動攻擊。總覺得興奮起來了。稍微期待一下吧。