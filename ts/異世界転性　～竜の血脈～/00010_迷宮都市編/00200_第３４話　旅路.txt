和科爾多巴軍的戰鬥過了一個月。
莉雅們一行人、向著暗黑迷宮最近的都市、傑巴古前進著。
那場戰鬥結束後半個月、莉雅們從那個村子出發。所幸、村民們沒有出現傷亡。可說是完全勝利。
村長還是不安的臉、但也不可能永遠待在這邊。而且都過了半個月連一點消息都沒有、那場戰鬥被科爾多巴內部處理掉了吧。責任者也死了。

路上沒什麼可以說的。
山岳部分出現山賊、結果反而被地獄獵犬襲擊了。確實、地獄獵犬一匹就有打倒整個集團的戰鬥力。
與此同時、襲擊有４人的魔法使一行人而言戰力是不足的。對著乞求饒命的山賊們、莉雅毫不留情的全砍了。
戰鬥的時候不說、莉雅竟然殺了在投降的山賊。至少薩基和瑪露沒有做。

「小姐、不用這樣沾污自己的手也可以啊？」

有時卡洛斯會這樣說、莉雅只是苦笑。

「沾污自己的手、我認為是高貴的人的宿命」
「但是⋯」
「而且、卡洛斯」

莉雅這次、以真正的笑容說著。

「我覺得、我好像喜歡上殺人了」

卡洛斯只能絶句放棄。

「安心吧。必須要殺的時候才會做、不該死的人是不會殺的」

當領悟到那真的是認真在說的時候、卡洛斯背上冷汗直流。
像貓真正地溺愛著瑪露的莉雅。
害怕同伴受傷、而自己戰鬥的莉雅。
對奴隷狩獵憤怒、不顧危險想要幫助救出的莉雅。
那樣的她、說喜歡殺人。

獨自一人留在心頭的事。但是不論如何也無法理解、卡洛斯只好找露露商量了。
露露不用說、是和莉雅在一起最久的人。正因如此、也可以說是最了解莉雅本質的人。
想了一會後、露露嘆了口氣。

「是在擔心莉雅吧。還有她啊、對無害的人是絶對不會動手的」
「擔心、是嗎？」
「你也是、討厭斬了已經沒有抵抗力的山賊是嘛？」

確實是這樣、卡洛斯搔了搔頭。

───

稍微說一下、其實、莉雅斬了那些人的理由、是更單純的。
只是想試刀而已。

「姆唔⋯袈裟斬而以刀刃就損傷⋯⋯是甚麼不夠」

莉雅試驗著、想像出來的刀不是像虎徹這種古時候做的刀。
前世即使日本刀、特別是被稱為古刀的時期、被太刀深深的打動、它的作法幾乎不知道。最初使用鐵的精鍊方法和謎一樣。
說起來、虎徹的製作方法每個時期都不同、實際上被認為是手的延長來揮舞的刀、認真地創造出來。

「所以說、有甚麼重點嗎？」

在這種時候莉雅就來商量了、卡洛斯和露露還有薩基也困擾著。

「不、俺只知道遊戲和漫畫的、而真刀的知識只有一點」

認真的談過後明白了、感到困擾。

「大概像那樣的作品、都是奧里哈鋼和秘銀做的武器」

奧里哈鋼和秘銀都是魔法金屬。莉雅得到的創世魔法、無法產生出那種素材。

「堅鋼礦石怎樣？」
「在這個世界、那就是鐵合金吧」
「那～、緋緋色金」
「比秘銀還要柔軟。和魔法相性很好」
「其他的有點想不起來阿。前世還有鈦合金和鎢鋼之類的⋯」
「原本日本刀就是鐵和鈦合金做出來的東西啊」
「是哦？俺以為只有鐵而已」

意外的是薩基好像不知道、好像一般都是這樣。即使是劍術家、刀的好壊姑且不論、如此執著於素材的人並不多。

「嗯啊、日本刀基本、是鋼做出來的。這種鋼是所謂的碳素鋼、比鐵還要硬。而碳素的含量多寡也會影──」
「啊啊、等等！太多專業的話俺聽不懂啊」

薩基的專長是徹底的偏向魔法、即使魔法這個系統很難學習。從菈比琳斯收到的魔導全書學習、不明白的地方就問露露或莉雅、之後是和前世的科學知識和想像做結合。
確實前世對遊戲和輕小說還有動畫本身也是很喜愛的、日本刀的魅力自然是知道的、但內容卻不太清楚。

「基本是鐵和炭素、鈦、鉻想辦法應該可以做出來⋯」

嘮嘮叨叨的嘟噥著、莉雅又回去做刀了。

───

旅途順利的持續著。

每天都有魔物襲擊、每次都擊退、等級也上升著。
最值得一提的是、瑪露的精靈魔法終於熟練了。
風的魔法把敵人切裂、土的魔法貫穿、火的魔法燃燒、冰的魔法凍結。
通常比起系統化的魔法理論更加模糊、但使用卻更方便。精靈魔法被認為、確實和魔法是不同的東西、說是精靈術還比較好。

薩基也熟練了魔法。
原本是時空魔法和火魔法特化的、兒物理魔法、術理魔法都是和露露學的、其他系統的技能等級也都上升了。據說是魔法的天賦這個天賦、效果十分顯卓。

「照這樣子看、很快我就沒有東西可以教你了」

雖然露露這麼說、但輔助魔法的效果更好、能更快的展現出來。實戰經驗甚麼的也更優秀吧。

構築前線的戰士們、等級當然也提高了。特別是卡洛斯、和手上的魔劍相當親密的樣子、會陶醉地凝視著刀刃、就像是莉雅的同類。
紀咕現在使用的戰槌變輕了、是莉雅重新更改重量後生產出來的。

「剛入手的時候沒有這樣想過、這真是方便啊、創世魔法」

在村子和科爾多巴軍戰鬥時也是、沒有任何阻礙就生產了相當數量的槍、在旅行期間、也產出了好幾把刀。
太過巨大了、只要肯砸下魔力、不是太複雜的構造就能一直生產出來。

給簡陋的馬車車軸替換成金屬製和加上彈簧後、現在正大活耀著。武器創造而言、正確來說是金屬製的物品更容易製造。
如果薩基沒說的話可能就不會注意到。
順便說、想要做貴金屬的話需要更多魔力。好像越稀有的金屬、就需要創造的魔力。金屬以外的皮系列的話、消耗更激烈。

「儘管如此、這樣就不用擔心錢了。這個、不隱藏起來的話肯定會發生騷動」

薩基說著、莉雅卻不是這樣想。就秘銀和奧里哈鋼而言、比黃金更珍貴的魔法金屬是做不出來的。
試著做著金幣、純金的卡薩莉雅金幣簡單的就做出來了。不考慮組成的話、比製作刀更簡單。

「這些錢留著、打造把好刀的話？」
「金錢能解決的話、一開始就會這樣做吧。原本鍛冶的刀就不多」

卡薩莉雅的劍、基本都是西洋風的直劍。雖然也有彎刀、但可以被稱為刀的卻是很少。
即使有再多錢、沒有的東西也買不到。就是這樣。

「這個世界、有哪個國家在使用刀的嗎」

對這類的事、薩基並不清楚。莉雅過去調查過、但是絶望了。

「主要是在南部。從其他的大陸傳入的。現在的刀、大多是彎刀而不是刀」

卡薩莉雅王國也是、很多是從極東的地方傳入的。當然彎刀這個武器的特性、在大陸中有一定程度的技術在傳播著、果然本家還是在大陸南部吧。
但是、這附近連一點線索都沒有。

「拜託矮人嗎⋯」

在山岳的部分有矮人的村落存在。特別是卡薩莉雅附近的山岳部分、為了採集優質的礦石、很多矮人住在那裏。
矮人的治金技術是其他種族比不上的。事實上卡薩莉雅王家的家傳之寶的武器、很多都是經由矮人之手做出來的。
莉雅愛用的十字槍、不是寶物而是由矮人謹制的作品、是纏著父王才得到的。

附近也有矮人的自治都市、但如果要去就必須回頭。有點麻煩吶。

「暗黑迷宮之後、是矮人的城市吶。在這之前、必須讓瑪露回去故鄉呢⋯」

莉雅在說的時候、感到有點寂寞。旅途的結束、就是和瑪露離別的意思。至少、莉雅打算遵守約定。

「我稍微延後也沒關係⋯」

在車夫台的瑪露說。在那旁邊、薩基和莉雅交談著。

「要遵守約定。無論如何可是不能打破對方的約定哦」

雖然作為奴隷被買、但瑪露這幾個月來、完全沒有覺得自己被當成奴隷。無論再怎麼自卑、也只是像姊姊在戲弄妹妹、不會認為是在當服侍任性公主的家臣的樣子。

「啊、看到了」

打斷談話、薩基說。

沿著山塊之間的道路前方、險峻的山峰山腳下、到暗黑迷宮前、最後的城市。
傑巴古。道路的終點站。人口約２萬人、其中大半、果然是依靠著迷宮生存著。
通常都是組成商隊來造訪。像莉雅們這樣只有少數人就來的、都是有相當本領的。

「總之、必須要找能洗澡的旅館」

對著莉雅一如既往的話、一行人露出苦笑。